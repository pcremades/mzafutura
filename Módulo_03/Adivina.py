#!/usr/bin/env python3
# Importamos la biblioteca "random"
import random

# Utilizamos la función "randint" para obtener
# un número aleatorio entre 1 y 100 
number = random.randint(1, 100)

guess = 0
while guess != number:
    guess = int(input("Ingrese un número: "))
    if guess > number:
        print("Más chico.")
    elif guess < number:
        print("Más grande.")

print(f"Muy bien!!! El número es {number}")
