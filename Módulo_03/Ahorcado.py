#!/usr/bin/env python3
import random
import csv

palabras = ['ahoracado', 'salida', 'entrada']


palabra = ''
lives = 10

print('_ '*len(palabra))

abecedario = ['abcdefghijklmnopqrstuvwxyz']

letrasFaltan = set(palabra)
letrasEstan = set()

while len(letrasFaltan) > 0 and lives > 0:
    guess = input('Ingrese una letra: ').lower()
    if guess in letrasFaltan:
        letrasFaltan.remove(guess)
        letrasEstan.add(guess)
        wordList = [x if x in letrasEstan else '_' for x in palabra]
        print(' '.join(wordList))
    else:
        print('NO')
        lives -= 1

if lives > 0:
    print(f"Ganaste! La palabra era {palabra}")
else:
    print(f'Perdiste, la palabra era {palabra}')


# palabras = []
# with open("lemario-general-del-espanol.txt", "r") as file:
#     csv_reader = csv.reader(file)
#     for row in csv_reader:
#         for val in row:
#             palabras.append(val)

# print(len(palabra))
# while len(palabra) < 5 and '-' not in palabra:
#     palabra = random.choice(palabras)
