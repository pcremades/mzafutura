#!/usr/bin/env python3
import os
import sys
from time import sleep
import pyfiglet

os.system('cls')

# print(os.path.dirname(__file__))
print(pyfiglet.figlet_format('La Llamada',font='wavy'))
print('\n')
nombreJugador = input('Ingresa tu nombre: ')
sleep(2)
os.system('cls')

f =  open('call.txt')
print(f.read())
f.close()

print(f'- Operadora: Hola {nombreJugador}. Hemos recibido una extaña llamada.\n\
    La voz era poco clara, no pudimos entender el mensaje. Podría haber una persona en peligro.\n\
    Hemos podido rastrear la llamada y tenemos la dirección. Ahora te la envío.\n\
    Preséntate cuanto antes en el domicilio para investigar. Comunícanos las novedades.\n')

sleep(10)
os.system('cls')
f =  open('house.txt')
print(f.read())
f.close()

print(f'\nLlegas al domicilio que te indicaron. Es una antigua casa de adobe.\n\
Parece abandonada, pero por las ventanas se percibe luz.\n\
Golpeas insistentemente la puerta pero nadie responde. Intentas abrir la puerta\n\
pero está cerrada con llave.\n\
Frente a la puerta hay una alfombra que dice "Home". La levantas y para tu sorpresa\n\
encuentras una llave debajo.\n')

stage1Words = ['llave', 'puerta']
stage = 1

while stage == 1:
    usuario = input('¿Qué quieres hacer? ')

    if all(x in usuario for x in stage1Words):
        stage = 2
    else:
        print('No puedes hacer eso.')

sleep(1)
os.system('cls')

# Stage 2: adentro
f =  open('stairs.txt')
print(f.read())
f.close()

print(f'\nHas abierto la puerta.\nIngresas a la casa y escuchas una voz extraña que viene de arriba\n\
Te debates entre subir e investigar lo que está ocurriendo arriba\n\
y volver a tu vehículo y llamar a la central para que llamen refuerzos.\n')

stage2Words1 = ['auto', 'vehículo', 'volver', 'ayuda']
stage2Words2 = ['subir', 'arriba', 'escaleras']
while stage == 2:
    usuario = input('¿Qué quieres hacer? ')

    if any(x in usuario for x in stage2Words1):
        stage = 3
    elif any(x in usuario for x in stage2Words2):
        stage = 4
    else:
        print('No puedes hacer eso.')

sleep(2)
os.system('cls')

# Stage 3: Final 1 - ayuda
if stage == 3:
    print(f'\nRegresas a tu vehículo para pedir ayuda por la radio\n\
pero descubres que no tienes señal. El miedo te invade y decides\n\
regresar por donde viniste e inventar una excusa a tus superiores.\n')

# Stage 4: Final 2 - arriba
elif stage == 4:
    f =  open('abu.txt')
    print(f.read())
    f.close()
    print(f'\nSubes por las escaleras y te encuentras en un pasillo largo y angosto\n\
Al final del pasillo hay una habitación. Ingresas en la habitación te encuentras\n\
con una anciana. Está tratando de rescatar a su gatito que se encuentra sobre un árbol\n\
y no puede saltar hasta la ventana.\n\
La ayudas a rescatar a su gatito y te invita a tomar unos mates con galletas recién horneadas.')

sleep(5)
print(pyfiglet.figlet_format('FIN',font='wavy'))
sys.exit()
