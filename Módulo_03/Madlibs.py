#!/usr/bin/env python3

sustantivo_1 = "mujer"
sustantivo_1 = input("Ingrese un sustantivo: ")
verbo_1 = "peinándose"
verbo_1 = input("Ingrese un verbo: ")
sustantivo_2 = "peine"
sustantivo_2 = input("Ingrese un sustantivo: ")

print(f"Sobre una torre había una {sustantivo_1}, de túnica blanca, {verbo_1} \
la cabellera, que le llegaba a los pies. El {sustantivo_2} desprendía sueños, \
con todos sus personajes: los sueños salían del pelo y se iban al aire. \
(Eduardo Galeano, CELEBRACIÓN DE LA FANTASÍA)")
