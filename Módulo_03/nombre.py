#!/usr/bin/env python3

import random

numero = random.randint(1, 20)

numUsuario = 0
while numUsuario != numero:
    numUsuarioTxt = input("Adivina que número estoy pensando: ")
    numUsuario = int(numUsuarioTxt)

    if numUsuario > numero:
        print("No. Es más chico.")
    elif numUsuario < numero:
        print("No. Es más grande.")
    else:
        print(f"Adivinaste! El número era {numero}")
