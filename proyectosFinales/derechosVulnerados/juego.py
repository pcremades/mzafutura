# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
from time import sleep
import pyfiglet
from sys import platform


if platform == "linux" or platform == "linux2":
    borraPantalla = 'clear'
elif platform.__contains__("win"):
    borraPantalla = 'cls'

os.system(borraPantalla)

f = open('front1.txt', 'r')
print(f.read())
f.close()
sleep(2)

os.system(borraPantalla)
f = open('front2.txt', 'r')
print(f.read())
f.close()
sleep(2)

nombreJugador = input('Ingresa tu nombre: ')
sleep(2)
os.system(borraPantalla)

print(f"Hola {nombreJugador}! Acompáñame a esta historia donde conocerás a un preadolescente pasando un mal rato…")

sleep (4)
os.system(borraPantalla)

print("Llegó el día de entrega de libretas. Sabes que este cuatrimestre no te ha ido muy bien,\n\
 por lo que estás nervioso al no saber exactamente cuáles serán tus notas. Esperas impaciente que\n\
 salgan tus padres de dirección, rogando que todo salga bien. Sientes el sonido de la puerta abrirse\n\
 y lo primero que ves es a tu padre muy enojado, y justo en ese momento te das cuenta que lo has vuelto a defraudar\n\
 (seguro reprobé todo y ahí viene de nuevo otra de sus regañadas- te dices a ti mismo).")

sleep (10)

print('\n')
print("-Papá: ¡¡Markus!! No ves el esfuerzo que estoy haciendo para pagarte los estudios y me venís con\n\
 estas mediocres notas… ¡Markus, te estoy hablando. Mírame cuando te hablo! - Alzando la voz- ")
sleep(6)
print('\n')
print("Markus lo ignora y mira hacia abajo")
print('\n')
sleep(2)
print("-Markus: Papá, me estás haciendo pasar vergüenza, por favor estamos en medio de la escuela.")

sleep(4)
print ("-Papá: ¿Yo te hago pasar vergüenza? Vos me das vergüenza, no trabajas, no haces nada en casa\n\
 y te la pasas encerrado en tu cuarto.Y me venís con esto!!! -mostrando la libreta de calificaciones")
print('\n')

sleep(4)
print ("El padre lo golpea con la libreta en la cabeza.")
print ("Markus empieza a llorar.")

print('\n')
print("opción a: Quedarte callado y fingir como que no escuchaste nada.\n\
opcion b: Llamar a un profesor para que interfiera en la situación.")

stage = 1

while stage == 1:
    usuario = input('¿Que deberias hacer como compañero de Markus? ').lower()
    if usuario == 'a':
        stage = 2
    elif usuario == 'b':
        stage = 3
    else:
        print('Eso no es una opción válida')

sleep(1)
os.system(borraPantalla)
    
#stage 2: opcion A
if stage == 2:
    print ("Como nadie dijo nada, el padre de Markus no solo le pegó con la libreta, lo abofeteó,\n\
lo tomó del brazo, arrastrándolo hacia afuera del lugar. Markus no volvió a la escuela al día siguiente.")
    print('\n')
    print ("Los compañeros preocupados por lo que Markus había faltado el día siguiente de la entrega de notas,\n\
 decidieron hablar con su profesora sobre lo ocurrido con él. Ella tomó la iniciativa de ayudarlo,\n\
 por lo que citó al padre a una reunión donde hablaron, pero eso no fue suficiente para que tomara conciencia.\n\
 Markus siguió padeciendo estos acontecimientos hasta su adultez.")

#stage 3: opcion B
elif stage == 3:
    print("Al haber llamado a la profesora, ella de inmediato interviene en la situación.\n\
El padre de Markus discute con ella, pero lo hace entender que su hijo tiene problemas,\n\
que él desconoce. Le explica que Markus tiene muchas inseguridades y no sabe cómo afrontarlas,\n\
sumado al miedo que le tiene a su padre, ya que no es la primera vez que pasa por esta situación violenta\n\
al “defraudarlo”. El señor se calma, se da cuenta que está actuando mal y decide hablar\n\
tranquilamente con Markus sobre lo que le está pasando.")
    print('\n')
    print ("El padre reflexiona sobre cómo trato a su hijo estos últimos años, siempre viendo todo lo malo de él,\n\
 sin ver el esfuerzo que hace día tras día para ser mejor o complacerlo. Por lo que decide cambiar y buscar ayuda profesional.\n")

print(pyfiglet.figlet_format("FIN", font="wavy"))

sleep(10)
print('Si te sientes identificado con este caso y necesitas ayuda\n\
puedes llamar a la línea 102 atendida por profesionales especializados.\n\
La línea 102 es un servicio telefónico gratuito y confidencial de escucha,\n\
contención y orientación para niños, niñas y adolescentes al que puedes llamar\n\
ante situaciones de vulneración de tus derechos.')
