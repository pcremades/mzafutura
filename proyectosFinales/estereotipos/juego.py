# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python3
import os
import sys
from time import sleep
import pyfiglet
from sys import platform


if platform == "linux" or platform == "linux2":
    borraPantalla = 'clear'
elif platform.__contains__("win"):
    borraPantalla = 'cls'

os.system(borraPantalla)

f = open('front1a.txt', 'r')
print(f.read())
f.close()
sleep(3)

os.system(borraPantalla)
f = open('front2a.txt', 'r')
print(f.read())
f.close()
sleep(2)

# print(os.path.dirname(__file__))
# print(pyfiglet.figlet_format('ESTEREOTIPOS DE LOS JOVENES',font='slant'))
print('\n')
nombreJugador = input('Ingresa tu nombre: ')
sleep(2)
os.system(borraPantalla)

print(f'Hola {nombreJugador}. ¿Cómo estás? ¡Te damos la bienvenida a nuestra historia! En este momento\n\
    eres el personaje principal, te llamas Matías, tienes 15 años y te encuentras en 3° año de la secundaria.\n\
    Es un día normal para ti, como de costumbre entras al curso, saludas a todos tus amigos y te diriges hacia el\n\
    fondo del salón donde está tu banco. Luego ingresa tu profesor de matemática e inicia la clase dando un tema nuevo\n\
    y comienza a escribir en el pizarrón. Termina de escribirlo y se sienta sin dar alguna explicación del tema. Matías no entiende\n\
    y toma la iniciativa de ir a preguntarle al docente, al llegar al escritorio le pide que le explique el tema y el profe le responde:\n\
    -¿Para qué?, si nunca haces nada!!!!\n\
    El profesor eleva su voz y dice:\n\
    -¿Ahora querés que te explique? Si ya termina la clase, quedan 20 minutos.\n\
    -¡Anda a sentarte a tu banco, por favor!\n\
    Matías sigue insistiendo en que le explique. El profe se enoja y lo agrede verbalmente.\n\
    Matías acostumbra contarle sus problemas e ideas a sus padres pero también suele ser sumiso...\n')
    
sleep(10)

stage1Words1 = ['nada', 'ignorar', 'callar', 'silenciar' , 'olvidar','sentarse']
stage1Words2 = ['responder', 'hablar', 'explicar', 'directora', 'contar', 'dialogar', 'contestar', 'docente', 'maestra', 'profesor', 'padres']
stage = 1

while stage == 1:
    usuario = input('¿Qué quieres hacer? ')

    if any(x in usuario for x in stage1Words1):
        stage = 2
    elif any(x in usuario for x in stage1Words2):
        stage = 3
    else:
        print('No puedes hacer eso.')

os.system(borraPantalla)
if stage == 2:
    print('Matías decide quedarse callado y sufre la misma situación en varias oportunidades.')
    print('FIN')
    exit()
elif stage == 3:
    print('Cuando Matías llega a su casa le cuenta lo sucedido a sus papás y ellos deciden ir a hablar con los directivos del colegio.\n\
    La directora interviene y habla de lo ocurrido con el docente y los padres de Matías.Estos llegan a un acuerdo,\n\
    este se trata de que el profesor explique los temas y todas las veces los alumnos lo necesiten para que puedan comprender.')
    sleep(10)
    print('\n')
    print('Al día siguiente el profesor viene predispuesto a explicar el tema que dió y no desarrolló la clase anterior. Cuando\n\
    empieza a exponer el tema observa a Matías dialogando con un compañero e interrumpiendo su clase entonces le llama la atención.')

stage3Words1 = ['seguir', 'charlar', 'insultar', 'burlar', 'nada', 'soguear', 'joder', 'hablar']
stage3Words2 = ['callar', 'atención', 'disculpar', 'disculpas', 'callarse']

while stage == 3:
    usuario = input('¿Qué quieres hacer? ')

    if any(x in usuario for x in stage3Words1):
        stage = 5
    elif any(x in usuario for x in stage3Words2):
        stage = 4
    else:
        print('No puedes hacer eso.')

os.system(borraPantalla)
#Final 1
if stage == 4:
    print('Matías le pide disculpas al profesor por interrumpirlo y comienza a prestar atención.')
    print('FIN')
#Final 2
elif stage == 5:
    print('Matías bardea al docente y sigue parleando; por consiguiente, el profe le da un ultimatum, pero Matías\n\
        no le hace caso y el profe termina bajándole puntos.')
    print('FIN')

sleep(1)
