# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python3
import os
import sys
from os import system
from sys import platform
from time import sleep
import pyfiglet

if platform == "linux" or platform == "linux2":
    borraPantalla = 'clear'
elif platform.__contains__("win"):
    borraPantalla = 'cls'

os.system(borraPantalla)

import animation

print('\n')
nombreJugador = input('Ingresa tu nombre: ')
sleep(2)
os.system(borraPantalla)

print(f'\nHola {nombreJugador}, eres un estudiante con un trabajo en Burger King de medio tiempo,\n\
ya completaste tu parte de trabajo de día y estas de regreso a tu departamento,pero en el camino\n\
ves a una persona golpeando a un perro porque le estaba ladrando.\n')

claves1a = ['ignorar', 'me voy', 'irse', 'ignorarlo', 'seguir', 'volver', 'trabajo']
claves1b = ['intervenir', 'detenerlo', 'hablar', 'regañar', 'amenazar']
escenario = 1

while escenario == 1:
    usuario = input('¿Qué haces en esa situación? ')

    if any(x in usuario for x in claves1a):
        escenario = 2
    elif any (x in usuario for x in claves1b):
            escenario = 7
    else:
        print('No puedes hacer eso.')
        
sleep(1)
os.system(borraPantalla)

# escenario 7: Final Bueno
if escenario == 7:
    print (f'\nIntervienes. Y le preguntas qué por qué le está pegando. El señor te dice que lo ha\n\
estado molestando mucho y que ya se cansó.\n')

    claves4 = ['enfrentarlo', 'detenerlo', 'regañarlo', 'hablar', 'llamar atención ']
 
    escenario = 7

    while escenario == 7:
        usuario = input('Ahora, ¿Qué haces en esa situación? ')

        if any(x in usuario for x in claves4): 
            escenario = 8
        else:
            print('No puedes hacer eso.')
        
    sleep(1)
    os.system(borraPantalla)
    
    # escenario 8: Final BUENO
    escenario == 8
    print(f'\nLe llamas la atención y llevas al perro a tu casa donde tratas de buscarle un hogar\n\
reguntando a familiares, amigos y lo viralizas a diferentes refugios protectores de animales.\n\
(a veces es bueno hacer las cosas por ti mismo, pero no siempre) \n')

    print(pyfiglet.figlet_format('FINAL BUENO ',font='big'))  
    sleep(15)
    os.system(borraPantalla)

# escenario 2:
elif escenario == 2:
    print(f'\nLo ignoras. Pero al día siguiente de camino al trabajo vuelves a ver al mismo perro\n\
de ayer herido.\n\
¿Qué haces?\n\
\n\
1_ Lo vuelves a ignorar.\n\
2_ Lo ayudas.\n')

claves2a = ['1']
claves2b = ['2']
while escenario == 2:
    usuario = input('¿Qué quieres hacer?(escribe un número) ')

    if all(x in usuario for x in claves2a):
        escenario = 3
    elif all(x in usuario for x in claves2b):
        escenario = 4
    else:
        print('No puedes hacer eso.')
    
sleep(1)
os.system(borraPantalla)

# escenario 3: Final Malo
if escenario == 3:
    print(f'\nAl final del día lo encuentras muerto y recuerdas todas las oportunidades que\n\
tuviste para ayudarlo, y evitar su muerte. (Si no tienes los recursos para ayudarlo por lo\n\
menos llama a la dirección de salud de la municipalidad de tu zona.)\n\
\n\
\n')
    print(pyfiglet.figlet_format('FINAL MALO',font='big'))    
    sleep(15)
    
# escenario 4: 
elif escenario == 4:
    print(f'\nAl tratar de ayudarlo descubres que su herida es muy grave, intentas llevarlo pero\n\
el animal no te deja. (Recuerdas que frente al miedo, los animales suelen ponerse agresivos).\n\
¿Qué haces? \n\
\n\
1_ Le cubres la cabeza con tu campera para que se tranquilice y no pueda morderte y así consigues\n\
llevarlo hasta tu casa.\n\
2_  Llamas a la dirección de salud de la municipalidad de tu zona y te vas a trabajar.\n')

claves3a = ['1']
claves3b = ['2']
while escenario == 4:
    usuario = input('¿Qué quieres hacer?(escribe un número) ')

    if all(x in usuario for x in claves3a):
        escenario = 5
    elif all(x in usuario for x in claves3b):
        escenario = 6
    else:
        print('No puedes hacer eso.')
        
sleep(1)
os.system(borraPantalla)

# escenario 5: Final Malo 2
if escenario == 5:
    print(f'\nAl llevártelo a tu casa agravas la herida causándole mucho dolor y adelantas su\n\
muerte. (Mover a un animal herido no es muy recomendable y menos si este no se deja, llama a\n\
algún especialista.(4498288)\n\
\n')
    print(pyfiglet.figlet_format('FINAL MALO 2',font='big'))    
    sleep(14)
# escenario 6: Final Especialistas
elif escenario == 6:
    print(f'\nLlegan los especialistas y lo tratan, salvando su vida, pero queda con secuelas. Y se lo\n\
llevan a un refugio.\n\
\n')
    print(pyfiglet.figlet_format('FINAL ESPECIA-    LISTAS  (4498287) ',font='big'))  
    sleep(10)
    os.system(borraPantalla)
